# Repo traitement image


## Principe générale :
Composé de 2 modules : 
**un module dédié à la validation des images**
    test si l'image envoyé est valide, c'est à dire si l'image contient un visage

**un module de traitement de l'image**
    dont le rôle est de recadrer l'image de façon
    à obtenir une photo de type photo d'identité.
Dans terminal :
```bash
python facial-cropping.py
```
#### Traites toutes les images dans le dossier ./testPics

**un module de reconnaissance d'image :** 
    basé sur OpenCv, ce module compare cette
    image à celle présente dans la base et établit ou non un lien entre notre image
    et celle correspondante dans la base

## Pour l'instant :
**Le seul module effectif est celui dédié à la validation des images.**
La fonctionnalité de base est de détecté les visages sur une photos puis de les
extraires. Ex : je fais tourner l'algo sur une photo d'anniversaire avec 5 personnes dont le
visage est apparent.--> Le programme détecte automatiquement les visages et
m'extraie dans 5fichiers jpg différents les visages des personnes.



### Dépendance : 
Pour le module dédié à la validation des images : 

- [OpenCv](http://opencv.willowgarage.com/wiki/) (à compiler)
```
sudo port install python27
sudo port install opencv +python27
sudo port select --set python python27
```
- Python et pip
- installer PIL (pip install Pillow) [tuto install](https://pypi.python.org/pypi/Pillow/2.1.0)
- extension python à instaler avec pip : [Watchog](https://pythonhosted.org/watchdog/)
```bash
pip install watchdog
```


I confirm that following these steps I can install Pillow on Mavericks 10.9.2 with XCode 5

```bash
brew install libtiff libjpeg webp littlecms
```
go to here https://pypi.python.org/pypi/Pillow/2.3.1 downalod the zip file and unzip it.

>3: open a Terminal window and go to Pillow-2.3.1 folder in Terminal.

>4: these two lines are extremely important because they will ignore the errors during installation of Pillow, without these two lines the setup cannot be finished (I am using python 2.7 so you may need to change whatever version you use):

```bash
sudo sed -i '' -e 's/-mno-fused-madd//g' /System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/_sysconfigdata.py
sudo rm /System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/_sysconfigdata.pyc
```
```
sudo python setup.py install
```



##État actuel du projet :
- reconnaissance des images fonctionnel
- cropping des visage avec export dans des dossiers

##Todo
- ajoutez fonction d'export avec uuid unique par dossier
- rajoutez tests pour sécuriser algo